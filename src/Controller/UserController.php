<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class UserController extends AbstractController
{
    public function __construct(private UserRepository $repo) {
    }
    #[Route('/api/user', methods: 'POST')]
    public function register(Request $request, SerializerInterface $serializer, UserPasswordHasherInterface $hasher): Response
    {
        $user = $serializer->deserialize($request->getContent(), User::class, 'json');
        if($this->repo->findOneBy(['email' => $user->getEmail()])) {
            return $this->json(['error' => 'User already exists'], Response::HTTP_BAD_REQUEST);
        }
        $hashedPassword = $hasher->hashPassword($user, $user->getPassword());
        $user->setPassword($hashedPassword);
        
        $user->setRole('ROLE_USER');

        $this->repo->save($user, true);

        return $this->json($user);
    }

    #[Route('/api/user', methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    
    #[Route('/api/user/{id}', methods: 'GET')]
    public function one(User $user)
    {

        return $this->json($user);
    }

    #[Route("/api/user/{id}", methods: 'DELETE')]
    public function delete(User $user)
    {
        $this->repo->remove($user, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route("/api/user", methods: ['PATCH', 'PUT'])]
    public function patch(Request $request, SerializerInterface $serializer)
    {
        try {
            $user= $this->getUser();
            $serializer->deserialize($request->getContent(), User::class, 'json', [
                'object_to_populate' => $user,
                'ignored_attributes'=> [
                    'password',
                    'role'
                ]
            ]);
            $this->repo->save($user, true);
            return $this->json($user);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }


    #[Route('/api/account')]
    public function restrictedZone() {
        
        return $this->json($this->getUser());
    }

    #[Route('/api/history', methods:'GET')]
    public function getUserHistory( OrderRepository $orderRepository): Response
{
    $products = $orderRepository->findBy(['user' => $this->getUser()]);

    return $this->json($products);
}
}


