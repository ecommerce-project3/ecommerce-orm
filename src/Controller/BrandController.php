<?php

namespace App\Controller;

use App\Entity\Brand;
use App\Repository\BrandRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;

#[Route('api/brand')]

class BrandController extends AbstractController
{
    public function __construct(private BrandRepository $repo)
    {
    }


    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }


    #[Route('/{id}', methods: 'GET')]
    public function one(Brand $brand)
    {

        return $this->json($brand);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer)
    {
        try {
            $brand = $serializer->deserialize($request->getContent(), Brand::class, 'json');
            $this->repo->save($brand, true);

            return $this->json($brand, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }


    }


    #[Route("/{id}", methods: 'DELETE')]
    public function delete(Brand $brand)
    {
        $this->repo->remove($brand, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route("/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(Brand $brand, Request $request, SerializerInterface $serializer)
    {
        try {

            $serializer->deserialize($request->getContent(), Brand::class, 'json', [
                'object_to_populate' => $brand
            ]);
            $this->repo->save($brand, true);
            return $this->json($brand);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }


}