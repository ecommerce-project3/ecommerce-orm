<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ImageRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;


#[Route('api/product')]

class ProductController extends AbstractController
{
    public function __construct(private ProductRepository $repo)
    {
    }


    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }


    #[Route('/{id}', methods: 'GET')]
    public function one(Product $product)
    {

        return $this->json($product);
    }


    #[Route('/category/{id}', methods: 'GET')]
    
        public function findByCategory(Request $request, int $id): Response
        {
            return $this->json($this->repo->findByCategory($id));

        }


    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer)
    {
        try {
            $product = $serializer->deserialize($request->getContent(), Product::class, 'json');
            $this->repo->save($product, true);

            return $this->json($product, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }


    }


    #[Route("/{id}", methods: 'DELETE')]
    public function delete(Product $product)
    {
        $this->repo->remove($product, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route("/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(Product $product, Request $request, SerializerInterface $serializer)
    {
        try {

            $serializer->deserialize($request->getContent(), Product::class, 'json', [
                'object_to_populate' => $product
            ]);
            $this->repo->save($product, true);
            return $this->json($product);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }

    #[Route('/search/{query}', methods: 'GET')]
    public function search(string $query): Response
    {
        $products = $this->repo->searchTitleOrDescription($query);
    
        if (count($products) === 0) {
            $products = [];
        }
    
        return $this->json($products);
    }

}