<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\Product;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;

#[Route('/api/order')]
class OrderController extends AbstractController
{

    public function __construct(private OrderRepository $repo, private ProductRepository $productRepo)
    {
        
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(Order $order)
    {

        return $this->json($order);
    }

    // #[Route(methods: 'POST')]
    // public function add(Request $request, SerializerInterface $serializer)
    // {
    //     try {
    //         $order = $serializer->deserialize($request->getContent(), Order::class, 'json');
            
    //         $this->repo->save($order, true);
    //         // $this->repo->setUser($this->repo->getUser(11));
            

    //         return $this->json($order, Response::HTTP_CREATED);
    //     } catch (ValidationFailedException $e) {
    //         return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
    //     } catch (NotEncodableValueException $e) {
    //         return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
    //     }



        
    // }

    #[Route(methods: 'POST')]
public function createOrder(Request $request, SerializerInterface $serializer)
{
    try{

 
    $data = json_decode($request->getContent(), true);

    // Création d'une nouvelle commande
    $order = new Order();
    $order->setDeliveryMode($data['deliveryMode']);
    $order->setStatus('En cours de préparation');
    $order->setEstimateDate(new \DateTime);
    $order->setUser($this->getUser());

    // Ajout des produits à la commande
    foreach ($data['product'] as $productData) {
        
        $product = $this->productRepo->find($productData['id']);

        $order->addProduct($product);
        $product->setAvailable(0);

       
    
    }

    $this->repo->save($order, true);
 

    return $this->json($order, Response::HTTP_CREATED);
    
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
}



    #[Route("/{id}", methods: 'DELETE')]
    public function delete(Order $order)
    {
        $this->repo->remove($order, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route("/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(Order $order, Request $request, SerializerInterface $serializer)
    {
        try {

            $serializer->deserialize($request->getContent(), Order::class, 'json', [
                'object_to_populate' => $order
            ]);
            $this->repo->save($order, true);
            return $this->json($order);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }


}