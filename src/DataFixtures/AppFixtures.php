<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use App\Entity\Order;
use App\Entity\Image;
use App\Entity\Category;
use App\Entity\Picture;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();



        //category
        $categories = [];
        $items = ["Hauts", "Bas", "Accessoires", "Chaussures", "Vestes"];
        // for ($i = 0; $i < 5; $i++) {
            foreach ($items as $item) {
                $category = new Category;
                $category->setLabel($item);
            if ($item == "Hauts") {
                $category->setSizeType("letterSize");
            } else if ($item == "Bas") {
                $category->setSizeType("numberSize");
            } else if ($item == "Accessoires") {
                $category->setSizeType("accessorySize");
            } else if ($item == "Vestes") {
                $category->setSizeType("numberSize");
            }  else if ($item == "Chaussures") {
                $category->setSizeType("shoeSize");
            }
            $manager->persist($category);
            $categories[] = $category;
            //brand
            $brands = [];
            for ($j = 0; $j < 1; $j++) {
                $brand = new Brand();
                $brand->setName($faker->randomElement(["Nike", "H&M", "Lidl", "Converse", "Vans"]));
                $manager->persist($brand);
                $brands[] = $brand;

                //product
                $products= [];
                for ($k = 0; $k < 10; $k++) {
                    $product = new Product();
                    $product->setAvailable(1);
                    $product->setStatus($faker->randomElement(["Usé", "Bonne condition", "Neuf"]));
                    $product->setPrice($faker->randomFloat(2, 1, 50));
                    $product->setDescription($faker->sentence(15));
                    $product->setBrand($brand);
                    $product->setCategory($category);
                    if ($item == "Hauts") {
                        $product->setName($faker->randomElement(["T-shirt", "Pull", "Chemise"]));
                        $product->setSize($faker->randomElement(["XS", "S", "M", "L", "XL"]));
                    } else if ($item == "Bas") {
                        $product->setName($faker->randomElement(["Jeans", "Pantalon cargo", "Pantalon chino"]));
                        $product->setSize($faker->randomElement(["34", "36", "38", "40", "42", "44", "46", "48"]));
                    } else if ($item == "Accessoires") {
                        $product->setName($faker->randomElement(["Boucles d'oreilles", "Bague", "Collier"]));
                        $product->setSize("Taille unique");
                    } else if ($item == "Vestes") {
                        $product->setName($faker->randomElement(["Parka", "Blouson", "Veste", "Manteau"]));
                        $product->setSize($faker->randomElement(["34", "36", "38", "40", "42", "44", "46", "48"]));
                    }  else if ($item == "Chaussures") {
                        $product->setName($faker->randomElement(["Baskets", "Bottes", "Chaussures à plateforme", "Mocassins"]));
                        $product->setSize($faker->randomElement(["36", "38", "40", "42", "44", "46", "48"]));
                    }
                    $manager->persist($product);
                    $products[]= $product;
                    //image
        for ($l = 0; $l < 3; $l++) {
            $picture = new Picture;
            $picture->setImage($faker->imageUrl(640,640,"clothes", true));
            $picture->setAlt($faker->sentence());
            $picture->setProduct($product);
            $manager->persist($picture);
        }
                }
            }
        }

        
        //user
        $users = [];
        for ($i = 0; $i < 10; $i++) {
            $user = new User;
            $user->setFirstName($faker->firstName());
            $user->setLastName($faker->lastName());
            $user->setRole($faker->randomElement(['ROLE_USER', 'ROLE_ADMIN']));
            $user->setEmail($faker->email());
            $user->setAddress($faker->address());
            $user->setPhone($faker->phoneNumber());
            $user->setPassword($faker->password());
            $manager->persist($user);
            $users[] = $user;
            //order
            for ($j = 0; $j < 3; $j++) {
                $order = new Order;
                $order->setEstimateDate($faker->dateTimeBetween('-1 year', 'now'));
                $order->setDeliveryMode($faker->randomElement(["Point relais", "A domicile"]));
                $order->setStatus($faker->randomElement(["En cours de préparation", "En cours de livraison", "Reçu"]));
                $order->setUser($user);
                $manager->persist($order);

            }
        }

        $manager->flush();
    }
}